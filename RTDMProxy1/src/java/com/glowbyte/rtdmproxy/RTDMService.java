package com.glowbyte.rtdmproxy;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Properties;
import java.util.Scanner;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.*;
import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.apache.log4j.PropertyConfigurator;

/**
 * Servlet implementation class RTDMService
 */
@WebServlet(asyncSupported = true, description = "Save SOAP Message")//urlPatterns = { "/RTDMService" })

public class RTDMService extends HttpServlet {

    Logger logger = Logger.getLogger(RTDMService.class.getName());
    private static final long serialVersionUID = 1L;
    private SOAPConnection connection;
    private String bpName, keyName, rtdmUrl, globalPath,
            deleteFiles, errorTemplatePath, setTimeout, errorFileName, timeOutFileName; //удалять файлы, путь до тимплейтов с эррор и таймаут, таймаут.
    private String responseFileName = "-1";
    private long startTimeApplication;
    private long endTimeApplication;
    private String responseFromRtdm_;

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    private void getProperties() throws IOException {
        //PropertyConfigurator propertyLog = new PropertyConfigurator();
        PropertyConfigurator.configure("log4j.properties");
        Properties propertiesApp = new Properties();
        try {
            //TODO timeout;
            //propertyLog.doConfigure(getServletContext().getResourceAsStream("/WEB-INF/log.properties"), null);
            propertiesApp.load(getServletContext().getResourceAsStream("/WEB-INF/config.properties"));
            globalPath = propertiesApp.getProperty("global_path");
            bpName = propertiesApp.getProperty("bp_name");
            keyName = propertiesApp.getProperty("key_name");
            rtdmUrl = propertiesApp.getProperty("rtdm_url");
            deleteFiles = propertiesApp.getProperty("delete_files");
            setTimeout = propertiesApp.getProperty("set_timeout");
            errorTemplatePath = propertiesApp.getProperty("error_template_path");
            errorFileName = propertiesApp.getProperty("error_file_name");
            timeOutFileName = propertiesApp.getProperty("timeout_file_name");

        } catch (IOException e) {
            System.err.println("ОШИБКА: Файл свойств отсуствует!");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        OutputStream os = resp.getOutputStream();
        os.write("Glowbyte.Team of professionals!".getBytes());

    }

    /**
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     * @throws UnsupportedEncodingException
     * @throws MalformedURLException
     */
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, UnsupportedEncodingException, MalformedURLException {

        try {
            this.getProperties();

            SOAPMessage incomingMessage = null;

            MessageFactory messageFactory = MessageFactory.newInstance();
            startTimeApplication = System.currentTimeMillis();
            double hashCode = Math.abs(Math.log10(Math.random()/Math.pow(10,20-Math.random())));
            
            try (ServletInputStream soapIS = req.getInputStream()) {

                try (PrintStream out = new PrintStream(new File(globalPath + "requestfront" + "_" + hashCode + ".xml"))) {
                    
                    incomingMessage = messageFactory.createMessage(null, soapIS);
                    incomingMessage.writeTo(out);
                    
                    logger.log(Level.WARN, "File" + " " + globalPath + "requestfront" + "_" + hashCode + ".xml" + " created");
                }
            } catch (IOException e) {
                //Cant write file
                logger.log(Level.ERROR, "Cannot write file!" + " " + e);
            } catch (SOAPException ex) {
                logger.log(Level.ERROR, "SOAP Exception" + " " + ex);
            }

            try {
                String dataToFront = createRequestToRTDM("requestfront" + "_" + hashCode + ".xml", resp);

                if (!responseFromRtdm_.contains("Data name=\"ResponseFileName\"")) {
                    throw new Error();
                }

                try (OutputStream outputStream = resp.getOutputStream()) {
                    String mimeType = "text/xml; charset=utf-8";
                    resp.setContentType(mimeType);
                    resp.addHeader("Content-Type", mimeType);
                    outputStream.write(dataToFront.getBytes());
                    outputStream.flush();

                    endTimeApplication = System.currentTimeMillis();

                    if (deleteFiles.startsWith("1")) {

                        deleteTempFiles(hashCode, responseFileName);
                        endTimeApplication = System.currentTimeMillis();
                        long applicantTime = endTimeApplication - startTimeApplication; // Время полной обработки заявки
                        logger.log(Level.WARN, "File " + responseFileName + "and" + "requestfront_" + hashCode + " " + "removed");
                        logger.log(Level.WARN, "Applicantion send to front at " + applicantTime + "ms " + responseFileName + " with removed");

                    } else {

                        endTimeApplication = System.currentTimeMillis();
                        long applicantTime = endTimeApplication - startTimeApplication; // Время полной обработки заявки
                        logger.log(Level.WARN, "Applicantion send to front at " + applicantTime + "ms " + responseFileName + " without removing");
                    }
                }

            } catch (SocketTimeoutException e) {

                try (OutputStream outputStream = resp.getOutputStream()) {

                    String timeOutErrorTemplate = errorTemplatePath + timeOutFileName; //TODO
                    String line;
                    String dataerror = "";

                    BufferedReader br2 = new BufferedReader(new FileReader(timeOutErrorTemplate));
                    while ((line = br2.readLine()) != null) {
                        dataerror += line + "\n";
                    }

                    String mimeType = "text/xml; charset=utf-8";
                    resp.setContentType(mimeType);
                    resp.addHeader("Content-Type", mimeType);

                    outputStream.write(dataerror.getBytes());
                    outputStream.flush();
                }

                logger.log(Level.ERROR, "Timeout Exception. File name is " + hashCode + " " + e);

            } catch (NoSuchAlgorithmException e) {
                //lolwut
            } catch (Error ex) {

                try (OutputStream outputStream = resp.getOutputStream()) {

                    String notContainsErrorTemplate = errorTemplatePath + errorFileName; //TODO
                    String line;
                    String dataerror = "";

                    BufferedReader br2 = new BufferedReader(new FileReader(notContainsErrorTemplate));
                    while ((line = br2.readLine()) != null) {
                        dataerror += line + "\n";
                    }

                    String mimeType = "text/xml; charset=utf-8";
                    resp.setContentType(mimeType);
                    resp.addHeader("Content-Type", mimeType);
                    outputStream.write(dataerror.getBytes());
                    outputStream.flush();
                }
                logger.log(Level.ERROR, "Not such \"ResponseFileName\" str in RTDM response " + ex);
            }

        } catch (SOAPException ex) {
            logger.log(Level.ERROR, "Bad input POST data (not SOAP) " + ex);
        }
    }

    public String createRequestToRTDM(String hashfilename, HttpServletResponse resp) throws MalformedURLException, IOException, UnsupportedEncodingException, NoSuchAlgorithmException, SocketTimeoutException {

        String xml = new Scanner(getServletContext().getResourceAsStream("/WEB-INF/RequestRTDMTemplate.xml")).useDelimiter("\\Z").next();
        xml = xml.replaceAll("BP_NAME", bpName).replaceAll("KEY_NAME", keyName).replaceAll("KEY_VALUE", hashfilename);

        int time = Integer.parseInt(setTimeout);

        HttpURLConnection connectionToRTDM = (HttpURLConnection) (new URL(rtdmUrl).openConnection());
        connectionToRTDM.setConnectTimeout(time);
        connectionToRTDM.setReadTimeout(time);
        connectionToRTDM.setDoOutput(true);
        connectionToRTDM.setDoInput(true);
        connectionToRTDM.setRequestMethod("POST");
        connectionToRTDM.setRequestProperty("SOAPAction", rtdmUrl);
        connectionToRTDM.setRequestProperty("Content-Type", "text/xml; charset=utf-8");

        OutputStreamWriter wr = new OutputStreamWriter(connectionToRTDM.getOutputStream());
        wr.write(xml);
        wr.flush();

        StringBuilder responseFromRTDM = new StringBuilder();
        byte[] buf = new byte[1024];
        int length;
        try (InputStream in = connectionToRTDM.getInputStream()) {
            while ((length = in.read(buf)) != -1) {
                responseFromRTDM.append(new String(buf, 0, length));
            }
        }

        //<ns2:Val>Success</ns2:Val>
        int startNormalTag = responseFromRTDM.indexOf("<ns2:Val>");
        int endNormalTag = responseFromRTDM.indexOf("</ns2:Val>");
        String fileNameFromRTDM = responseFromRTDM.substring(startNormalTag, endNormalTag).replaceAll("<ns2:Val>", "").trim();

        responseFromRtdm_ = responseFromRTDM.toString();
        responseFileName = fileNameFromRTDM;

        String line;
       
        String responseSOA = "";
        try (BufferedReader br2 = new BufferedReader(new FileReader(globalPath + fileNameFromRTDM))) {
            while ((line = br2.readLine()) != null) {
                responseSOA += line + "\n";
            }

        } catch (IOException ex) {
            try (OutputStream outputStream = resp.getOutputStream()) {
                String notSuchFileErrorTemplate = errorTemplatePath + errorFileName; //TODO
                String lines;
                String dataerror = "";
                BufferedReader br2 = new BufferedReader(new FileReader(notSuchFileErrorTemplate));
                while ((lines = br2.readLine()) != null) {
                    dataerror += lines + "\n";
                }

                String mimeType = "text/xml; charset=utf-8";
                resp.setContentType(mimeType);
                resp.addHeader("Content-Type", mimeType);
                outputStream.write(dataerror.getBytes());
                outputStream.flush();

            }

            logger.log(Level.ERROR, "Not such response file.Expected " + fileNameFromRTDM + " " + ex);
        }
        connectionToRTDM.disconnect();
        return responseSOA;

    }

    void deleteTempFiles(double frontFileHash, String rtdmFileName) {

        Path rtdmFilePath = Paths.get(globalPath + rtdmFileName);
        Path frontFilePath = Paths.get(globalPath + "requestfront" + "_" + frontFileHash + ".xml");

        try {
            Files.delete(rtdmFilePath);
        } catch (NoSuchFileException x) {
            logger.log(Level.ERROR, "Not such file or directory" + x);
            System.err.format("%s: no such" + " file or directory%n", rtdmFilePath);
        } catch (DirectoryNotEmptyException x) {
            logger.log(Level.ERROR, "Not empty" + x);
            System.err.format("%s not empty%n", rtdmFilePath);
        } catch (IOException x) {
            logger.log(Level.ERROR, "IO Error" + x);
            System.err.println(x);
        }

        try {
            Files.delete(frontFilePath);
        } catch (NoSuchFileException x) {
            logger.log(Level.ERROR, "Not such file" + x);
            System.err.format("%s: no such" + " file or directory%n", frontFilePath);
        } catch (DirectoryNotEmptyException x) {
            logger.log(Level.ERROR, "Not empty" + x);
            System.err.format("%s not empty%n", frontFilePath);
        } catch (IOException x) {
            logger.log(Level.ERROR, "IO exception" + x);
            System.err.println(x);
        }
    }
}
