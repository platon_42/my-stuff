/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sendfilestortdm;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;
import java.util.Scanner;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author platon_mac
 */
public class SendFilesToRTDM {

    private String url, dirsxml, dirresponse, namefile, createdir, pathfiles;

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    private void getProperties() throws IOException {

        InputStream input = null;

        Properties propertiesApp = new Properties();
        String path = SendFilesToRTDM.class.getProtectionDomain().getCodeSource().getLocation().getPath();
        int lastIndexOf = path.lastIndexOf("/");
        input = new FileInputStream(path.substring(0, lastIndexOf) + "/config.properties");
        propertiesApp.load(input);
        url = propertiesApp.getProperty("url");
        dirsxml = propertiesApp.getProperty("sourcesir");
        dirresponse = propertiesApp.getProperty("responsedir");
        createdir = propertiesApp.getProperty("createdir");
        
    }

    public static void main(String[] args) throws IOException {
        SendFilesToRTDM SFM = new SendFilesToRTDM();
        SFM.sendRequest();
    }

    public void sendRequest() throws MalformedURLException, IOException {

        this.getProperties();

        File dir = new File(this.dirsxml);
        File[] directoryListing = dir.listFiles();
        System.out.println(Arrays.toString(directoryListing) + " Listing");
        if (directoryListing != null) {
            for (File child : directoryListing) {
                HttpURLConnection con = (HttpURLConnection) (new URL(url).openConnection());

                con.setRequestMethod("POST");
                con.setRequestProperty("Content-Type", "text/xml; charset=utf-8");
                con.setDoOutput(true);
                con.setDoInput(true);
                long start;

                try (final OutputStreamWriter wr = new OutputStreamWriter(con.getOutputStream(), "UTF-8")) {

                    start = System.currentTimeMillis();
                    //namefile = child.getAbsolutePath();
                    int lastpoint = child.getName().lastIndexOf(".");
                    namefile = child.getName().substring(0, lastpoint);
                    
                    pathfiles = child.getAbsolutePath();
                   
                    String fileContents = FileUtils.readFileToString(child, "UTF-8");
                    System.out.println(namefile + ".xml" + " send to RTDM");
                    wr.write(fileContents);
                    wr.flush();
                }

                int responseCode = con.getResponseCode();

                StringBuilder response;
                try (BufferedReader in = new BufferedReader(
                        new InputStreamReader(con.getInputStream()))) {

                    System.out.println(responseCode);

                    String inputLine;
                    response = new StringBuilder();
                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                }
                
                if (createdir.startsWith("0")) {
                    try (PrintStream out = new PrintStream(new FileOutputStream(pathfiles + "_response.xml"))) {
                        out.print(response);
                        long finish = System.currentTimeMillis() - start;
                        System.out.println(namefile + ".xml" + " processed to RTDM at " + finish);

                    }
                } else {
                    Files.createDirectory(Paths.get(dirresponse + namefile));
                    try (PrintStream out = new PrintStream(new FileOutputStream(dirresponse + namefile + "/" + "response" + namefile  + ".xml"))) {
                        out.print(response);
                        long finish = System.currentTimeMillis() - start;
                        System.out.println(namefile + " processed to RTDM at " + finish);
                    }
                }
            //con.disconnect();

            }

        } else {
            // Handle the case where dir is not really a directory.
            // Checking dir.isDirectory() above would not be sufficient
            // to avoid race conditions with another process that deletes
            // directories.
        }
    }
}
